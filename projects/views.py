from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": projects}
    return render(request, "projects/projects.html", context)


@login_required
def show_task(request, id):
    project = get_object_or_404(Project, id=id)
    task = Task.objects.filter(assignee=request.user)
    context = {
        "show_project": project,
        "show_task": task,
    }
    return render(request, "projects/details.html", context)


@login_required
def make_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
