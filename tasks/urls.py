from django.urls import path
from tasks.views import make_task, my_task

urlpatterns = [
    path("create/", make_task, name="create_task"),
    path("mine/", my_task, name="show_my_tasks"),
]
